<?php

namespace Pantheon\TerminusOptimizer\Commands;

use Pantheon\Terminus\Models\Site;
use Pantheon\TerminusBuildTools\Commands\BuildToolsBase;
use Pantheon\TerminusBuildTools\Commands\CommentAddPullRequestCommand;
use Pantheon\TerminusBuildTools\Commands\EnvDeleteCommand;

class InitializeCommand extends BuildToolsBase {

  /**
   * Initialize a pantheon multidev for CI operations.
   *
   * @authorize
   *
   * @command optimizer:initialize
   *
   * @param string $site_env_id Site & environment in the format `site-name.env`
   *
   * @option int $pr_id The PR number, if applicable.
   *
   * @usage <site.env> <machine_token> to create or update the given site and prepare for CI deployment.
   *
   */
  public function initialize($site_env_id, $options = ['pr_id' => NULL]) {
    list($site_id, $multidev_id) = array_pad(explode('.', $site_env_id), 2, null);
    $site = $this->getSite($site_id);
    $environmentExists = $site->getEnvironments()->has($multidev_id);
    $siteInfo = $site->serialize();
    $site_id = $siteInfo['id'];
    // $this->deleteBuildEnvPR($site_id);
    if ($environmentExists) {
      $this->refreshMultidevContentFromDev($site, $multidev_id);
    }
    else {
      $this->createMultidevEnvironment($site_id, $multidev_id);
      if ($options['pr_id']) {
        $this->addPRCommentWithMultidevLink($site, $multidev_id, $options['pr_id']);
      }
    }
  }

  protected function addPRCommentWithMultidevLink(Site $site, $multidev_id, $pr_id) {
    $commenter = new CommentAddPullRequestCommand($this->provider_manager);
    $site_id = $site->id;
    $site_name = $site->getName();
    $dashboard_url = "https://dashboard.pantheon.io/sites/{$site_id}#{$multidev_id}";
    $multidev_site_url = "https://{$multidev_id}-{$site_name}.pantheonsite.io/";
    $message = "Created multidev environment [$pr_id: Visit Site]($multidev_site_url) or [Visit Dashboard]($dashboard_url)";
    $commenter->commentAddPullRequest([
      'pr_id' => $pr_id,
      'message' => $message
    ]);
  }

  protected function refreshMultidevContentFromDev(Site $site, $multidev_id) {
    $from_env = $site->getEnvironments()->get('dev');
    $target = $site->getEnvironments()->get($multidev_id);
    // Kick off the files clone, and let it happen in background.
    // We don't need files for a successful db update.
    $from_name = $from_env->getName();
    $this->log()->notice(
      "Cloning files from {from_name} environment to {target_env} environment",
      ['from_name' => $from_name, 'target_env' => $target->getName()]
    );
    $target->cloneFiles($from_env);
    $this->log()->notice(
      "Cloning database from {from_name} environment to {target_env} environment",
      ['from_name' => $from_name, 'target_env' => $target->getName()]
    );
    $workflow = $target->cloneDatabase($from_env);
    while (!$workflow->checkProgress()) {
      $this->log()->notice('database clone in progress');
    }
    $this->log()->notice($workflow->getMessage());
  }

  protected function createMultidevEnvironment(Site $site, $multidev_id) {
    $workflow = $site->getEnvironments()->create($multidev_id, 'dev');
    while (!$workflow->checkProgress()) {
      $this->log()->notice('multidev create in progress');
    }
    $this->log()->notice($workflow->getMessage());
  }

  protected function login($token_string) {
    $tokens = $this->session()->getTokens();
    try {
      $token = $tokens->get($token_string);
    }
    catch (\Exception $e) {
      $this->log()->notice('Creating new session via machine token.');
      $tokens->create($token_string);
    }
    if (isset($token)) {
      $this->log()->notice('Logging in via machine token.');
      return $token->logIn();
    }
  }

  protected function deleteBuildEnvPR($site_id) {
    $env_delete = new EnvDeleteCommand($this->provider_manager);
    $env_delete->setSites($this->sites);
    $env_delete->setLogger($this->logger);
    $env_delete->setContainer($this->container);
    $env_delete->setSession($this->session);
    $env_delete->setConfig($this->config);
    $env_delete->deleteBuildEnvCI($site_id);
    $env_delete->deleteBuildEnvPR($site_id);
  }

}